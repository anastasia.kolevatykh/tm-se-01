package ru.kolevatykh.tm;

import ru.kolevatykh.tm.entity.Project;
import ru.kolevatykh.tm.entity.Task;
import ru.kolevatykh.tm.service.ProjectService;
import ru.kolevatykh.tm.service.TaskService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class TaskManager {
    private ProjectService projectService = new ProjectService();
    private TaskService taskService = new TaskService();

    private static final String help = "help: \tShow all commands."
            + "\nproject-clear: \tRemove all projects."
            + "\nproject-create: \tCreate new project."
            + "\nproject-list: \tShow all projects."
            + "\nproject-remove: \tRemove selected project." // todo
            + "\ntask-clear: \tRemove all tasks."
            + "\ntask-create: \tCreate new task."
            + "\ntask-list: \tShow all tasks."
            + "\ntask-remove: \tRemove selected task.";

    private void projectCreate(String name) {
        Project project = new Project(name);
        projectService.create(project);
        System.out.println("[OK]");
    }

    private void projectList() {
        projectService.readAll();
    }

    private void projectClear() {
        taskService.clearAll();
        projectService.clearAll();
        System.out.println("[Removed all projects with tasks.]");
    }

    private void projectRemoveByName(String name) {
        tasksRemoveByProjectName(name);
        projectService.removeByName(name);
        System.out.println("[Removed " + name + " project with tasks.]");
    }

    private String projectFindByName(String name) {
        if ((projectService.findByName(name)) == null) {
            System.out.println("[The project under this name does not exist!]");
            return null;
        }
        return (projectService.findByName(name)).getName();
    }

    private void taskCreate(String projectName, String title, String content) {
        long projectId = projectService.findByName(projectName).getId();

        Task task = new Task(projectId, projectName, title, content);
        taskService.create(task);
        System.out.println("[OK]");

    }

    private void taskList() {
        taskService.readAll();
    }

    private void tasksClear() {
        taskService.clearAll();
        System.out.println("[Removed all tasks.]");
    }

    private void tasksRemoveByProjectName(String name) {
        taskService.removeByProjectName(name);
    }

    private void taskRemoveByName(String projectName, String taskName) {
        taskService.removeByName(projectName, taskName);
    }

    public static void main(String[] args) {

        System.out.println("*** Welcome to task manager ***" +
                "\nType \"help\" for details.");
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

            String line;
            TaskManager main = new TaskManager();

            while ((line = reader.readLine()) != null) {
                if ("help".equals(line)) {
                    System.out.println(help);
                }
                if ("project-clear".equals(line)) {
                    System.out.println("[PROJECT CLEAR]");
                    main.projectClear();
                }
                if ("project-create".equals(line)) {
                    System.out.println("[PROJECT CREATE]"
                            + "\nEnter name: ");
                    String name = reader.readLine();
                    main.projectCreate(name);
                }
                if ("project-list".equals(line)) {
                    System.out.println("[PROJECT LIST]");
                    main.projectList();
                }
                if ("project-remove".equals(line)) {
                    System.out.println("[PROJECT REMOVE]"
                            + "\nEnter name:");
                    String name = reader.readLine();
                    main.projectRemoveByName(name);
                }
                if ("task-clear".equals(line)) {
                    System.out.println("[TASK CLEAR]");
                    main.tasksClear();
                }
                if ("task-create".equals(line)) {
                    System.out.println("[TASK CREATE]"
                            + "\nEnter project name: ");
                    String projectName = reader.readLine();

                    if (main.projectFindByName(projectName) != null) {
                        System.out.println("Enter title: ");
                        String title = reader.readLine();

                        System.out.println("Enter content: ");
                        String content = reader.readLine();
                        main.taskCreate(projectName, title, content);
                    }
                }
                if ("task-list".equals(line)) {
                    System.out.println("[TASK LIST]");
                    main.taskList();
                }
                if ("task-remove".equals(line)) {
                    System.out.println("[TASK REMOVE]"
                            + "\nEnter project name:");
                    String projectName = reader.readLine();

                    System.out.println("Enter task name:");
                    String taskName = reader.readLine();
                    main.taskRemoveByName(projectName, taskName);
                }

            }
            reader.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

    }
}

