package ru.kolevatykh.tm.entity;

public class Task {

    private long projectId;
    private String projectName;
    private String title;
    private String content;

    public Task(long projectId, String projectName, String title, String content) {
        this.projectId = projectId;
        this.projectName = projectName;
        this.title = title;
        this.content = content;
    }

    public String getProjectName() {
        return projectName;
    }

    public long getProjectId() {
        return projectId;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return "Task{ " +
                "project name: " + projectName +
                ", title: '" + title + '\'' +
                ", content:'" + content + '\'' +
                '}';
    }
}
