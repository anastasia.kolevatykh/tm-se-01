package ru.kolevatykh.tm.entity;

public class Project {
    private String name;

    private long id = System.nanoTime();

    public Project(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public long getId() {
        return id;
    }

    @Override
    public String toString() {
        return "name: " + name;
    }
}
