package ru.kolevatykh.tm.service;

import ru.kolevatykh.tm.entity.Project;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ProjectService {
    private List<Project> projects = new ArrayList<Project>();

    public void create(Project project) {
        projects.add(project);
    }

    public Project findByName(String name) {
        for (Project p : projects) {
            if (name.equals(p.getName())) {
                return p;
            }
        }
        return null;
    }

    public void readAll() {
        for (Project project : projects) {
            System.out.println("id: " + project.getId()
                    + ", name: " + project.toString());
        }
    }

    public void clearAll() {
        projects = new ArrayList<Project>();
    }

    public void removeByName(String name) {
        Iterator<Project> it = projects.iterator();
        while (it.hasNext()) {
            Project project = it.next();
            if (name.equals(project.getName())) {
                it.remove();
            }
        }
    }
}
